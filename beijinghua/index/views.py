from django.shortcuts import render, redirect

# Create your views here.
def index(request):
    x = ''
    if request.method == 'POST':
        ret = ''
        str = request.POST['s']
        # str = str[1: -1]
        s = str
        str = str.replace('儿', '').replace('你', '您')
        for i in str:
            ret = ret + i
            if i >= '0' and i <= '9':
                continue
            if i >= 'a' and i <= 'z':
                continue
            if i >= 'A' and i <= 'Z':
                continue
            if i in "'/!@#$%^&*()_+":
                continue
            if i != '\n':
                ret = ret + '儿'
        if ret[-1] != '儿':
            ret = ret + '儿'
        x = ret

    return render(request, 'index/index.html', locals())
