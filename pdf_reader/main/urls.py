from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('file_downloader', views.file_downloader, name='file_downloader'),
    path('uploadFile', views.uploadFile),
]