from django.shortcuts import render, redirect
from django.http import FileResponse, HttpResponse
import os

# basic_path = 'pdf_reader/files'
basic_path = 'files'

def index(request):
    t = []
    for _, _, c in os.walk(basic_path):
        t = c
    t = sorted(t)
    return render(request, 'main/index.html', {'content': t})

def file_downloader(request):
    filename = request.GET.get('filename', -1)
    if filename == -1:
        return redirect('/')
    t = []
    for _, _, c in os.walk(basic_path):
        t = c
    t = sorted(t)
    #   filename = t[filename]
    f = open(basic_path + '/' + filename, 'rb')
    return FileResponse(f)

def uploadFile(request):
    if request.method == 'POST':
        myFile = request.FILES.get('myfile', None)
        if not myFile:
            return HttpResponse('no files for upload')
        des = open(basic_path + '/' + myFile.name, 'wb+')
        for chunk in myFile.chunks():
            des.write(chunk)
        des.close()
        return redirect('/')