import React, { useState } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import Layout from '../Layout/Layout';
import axios from 'axios';
import SimpleSnackbar from '../Tools/SimpleSnackbar';
import DjangoCSRFToken from 'django-react-csrftoken';
import copy from 'copy-to-clipboard';

export default function Beijinghua() {
  const [inputStr, setInputStr] = useState('');

  const [warningMessage, setWarningMessage] = useState('init');
  const [severity, setSeverity] = useState('info');
  const [loading, setLoading] = useState(false);
  const [snackBarOpen, setSnackBarOpen] = useState(false);
  const [result, setResult] = useState('');

  const serverConfig = {
    baseUrl: `http://${window.location.host.replace(':3000', ':8000')}`,
  };

  const submit = (e) => {
    e.preventDefault();
    var message = '';
    var severity = '';
    var _result = '';
    var loading = true;
    let url = new URL('api/beijinghua/', serverConfig.baseUrl);
    let formData = new FormData();
    formData.append('input_str', inputStr);

    axios({
      method: 'POST',
      url: url,
      data: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((response) => response.data)
      .then((data) => {
        switch (data['data']) {
          case 'str is None':
            message = '请输入要转换的文本';
            severity = 'warning';
            loading = false;
            break;
          case 'Success':
            message = '转换成功';
            severity = 'success';
            loading = false;
            _result = data['output_str'];
            break;
          case 'Error':
            message = '接口catch到未知错误';
            severity = 'error';
            loading = false;
            break;
          default:
            message = '接口返回未知错误';
            severity = 'error';
            loading = false;
        }
      })
      .catch(function (e) {
        console.log('error: ', e);
        message = 'jsx catch到未知错误';
        severity = 'error';
        loading = false;
      })
      .finally(() => {
        setWarningMessage(message);
        setSeverity(severity);
        setLoading(loading);
        setSnackBarOpen(true);
        setResult(_result);
      });
  };

  const handleSnackBarClose = () => {
    setSnackBarOpen(false);
  };

  const copyText = () => {
    copy(result);
    setWarningMessage('复制成功');
    setSeverity('Success');
    setSnackBarOpen(true);
  };

  return (
    <Layout>
      <Grid container direction="row" spacing={2}>
        <Grid item>
          <br />
        </Grid>
        <Grid item direction="column" justify="flex-start" alignItems="flex-start">
          <Grid item>
            <Typography variant="h6" component="h2" gutterBottom>
              北京话生成器
            </Typography>
            <form onSubmit={submit}>
              <DjangoCSRFToken />
              <TextField
                type="input"
                inputStr={inputStr}
                id="inputStr"
                name="inputStr"
                label="请输入要转化的文本"
                multiline
                fullWidth
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                value={inputStr}
                onChange={(e) => {
                  setInputStr(e.target.value);
                }}
              />
              <Grid>
                <label htmlFor="contained-button-file">
                  <div>
                    <Button variant="contained" color="primary" type="submit" disabled={loading}>
                      提交
                    </Button>
                    {loading && <CircularProgress size={24} />}
                  </div>
                </label>
              </Grid>
            </form>
            <br />
            <Grid>
              {result == '' ? (
                <div />
              ) : (
                <Grid>
                  <Typography variant="body1" gutterBottom>
                    {result}
                  </Typography>
                  <Button variant="contained" color="primary" onClick={copyText}>
                    点击复制
                  </Button>
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <SimpleSnackbar
        snackBarOpen={snackBarOpen}
        warningMessage={warningMessage}
        severity={severity}
        handleSnackBarClose={handleSnackBarClose}
      />
    </Layout>
  );
}
