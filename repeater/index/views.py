from django.shortcuts import render
from random import randrange

# Create your views here.
def index(request):
    x = ''
    if request.method == 'POST':
        emoji_selected = request.POST['emoji']
        str = request.POST['s']

        if str == '':
            x = ''
        else:
            ret = ''
            if emoji_selected == 'wechat':
                emoji = '[色]'
            elif emoji_selected == 'emoji':
                emoji = '😍'
            elif emoji_selected == 'weibo_se':
                emoji = '[色]'
            elif emoji_selected == 'weibo_tianping':
                emoji = '[舔屏]'
            s = str
            ret = str + emoji + '我的' + str + emoji + emoji
            ret = ret * randrange(5, 10)
            x = ret

    return render(request, 'index/index.html', locals())
